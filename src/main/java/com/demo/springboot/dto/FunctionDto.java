package com.demo.springboot.dto;

import java.util.List;

public class FunctionDto {
    private Double a;
    private Double b;
    private Double c;

    public FunctionDto(Double a , Double b, Double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public FunctionDto() {

    }

    public Double getA() {
        return a;
    }

    public void setA(Double a) {
        this.a = a;
    }

    public Double getB() {
        return b;
    }

    public void setB(Double b) {
        this.b = b;
    }

    public Double getC() {
        return c;
    }

    public void setC(Double c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "FunctionDto{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
