package com.demo.springboot.dto;

public class DtoX {
    private Double p;
    private Double q;
    // zamiast p i q bylo x1 i x2 w tedy zwracalo miejsca zerowe !!!!!

    public DtoX(Double p, Double q) {
        this.p = p;
        this.q = q;
    }

    public DtoX(){

    }

    public Double getP() {
        return p;
    }

    public void setP(Double p) {
        this.p = p;
    }

    public Double getQ() {
        return q;
    }

    public void setQ(Double q) {
        this.q = q;
    }

    @Override
    public String toString() {
        return "DtoX{" +
                "p=" + p +
                ", q=" + q +
                ", W(" + p +","+q + ")"+
                '}';
    }
}
