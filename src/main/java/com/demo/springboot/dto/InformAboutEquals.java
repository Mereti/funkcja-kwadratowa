package com.demo.springboot.dto;

import java.util.Arrays;

public class InformAboutEquals {

    private String[] alerts = new String[]{
            "podaj a",
            "podaj b"
    };


    public InformAboutEquals(String[] alerts) {
        this.alerts = alerts;
    }
    public InformAboutEquals(){

    }

    public String[] getAlerts() {
        return alerts;
    }

    public void setAlerts(String[] alerts) {
        this.alerts = alerts;
    }

    @Override
    public String toString() {
        return "InformAboutEquals{" +
                "alerts=" + Arrays.toString(alerts) +
                '}';
    }
}
