package com.demo.springboot.service;

import com.demo.springboot.dto.DtoX;
import com.demo.springboot.dto.FunctionDto;
import com.demo.springboot.service.interfaces.Service;

import java.util.List;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

@org.springframework.stereotype.Service
public class ServiceImplementation implements Service {
   private DtoX listOfNumber ;

    @Override
    public DtoX CheckFunction(FunctionDto functionDto) {
        listOfNumber = new DtoX();

        Double a = functionDto.getA();
        Double b = functionDto.getB();
        Double c = functionDto.getC();
        Double x1 ;
        Double x2;
        Double p;
        Double q;
        Double delta = pow(b,2) - (4*a*c);

        if(delta > 0){
            p = (-b)/(2*a);
            q = (-delta)/(4*a);
            listOfNumber.setP(p);
            listOfNumber.setQ(q);

        }else if(delta == 0){
            x1 = (-b)/(2*a);
            x2 = null;
            listOfNumber.setP(x1);
            listOfNumber.setQ(x2);

        }else{ x1= null;
           x2 = null;
            listOfNumber.setP(x1);
            listOfNumber.setQ(x2);
        }
            return listOfNumber;

    }
}
