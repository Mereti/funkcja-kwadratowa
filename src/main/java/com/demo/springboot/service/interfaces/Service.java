package com.demo.springboot.service.interfaces;

import com.demo.springboot.dto.DtoX;
import com.demo.springboot.dto.FunctionDto;


public interface Service {
    DtoX CheckFunction (FunctionDto functionDto);

}
