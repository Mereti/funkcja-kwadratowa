package com.demo.springboot.rest;

import com.demo.springboot.dto.DtoX;
import com.demo.springboot.dto.FunctionDto;
import com.demo.springboot.dto.InformAboutEquals;
import com.demo.springboot.service.ServiceImplementation;
import com.demo.springboot.service.interfaces.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
public class DocumentApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);
@Autowired
    private Service service;//tutaj implementujemy SERVIS
    private FunctionDto functionDto;

    @CrossOrigin
    @GetMapping("/api/math/quadratic-function{a}{b}{c}")
    public ResponseEntity<DtoX> getFunctionDto(@RequestParam(value="a") Double a, @RequestParam(value="b") Double b, @RequestParam(value = "c") Double c ){
     /*   List<Double> lista = new ArrayList();
        lista.add(a);
        lista.add(b);
        lista.add(c);*/

        functionDto = new FunctionDto(a,b,c);
        functionDto.setA(a);
        functionDto.setB(b);
        functionDto.setC(c);

        return  new ResponseEntity<DtoX>(service.CheckFunction(functionDto), HttpStatus.OK);

       // return  new ResponseEntity<DtoX>(service.CheckFunction(lista), HttpStatus.OK);
    }

    @PostMapping(value="/api/math/lol")
    public ResponseEntity<Object> getFunctionDto(@RequestBody FunctionDto functionDto) {
        LOGGER.info("--- a: {}",functionDto.getA());
        LOGGER.info("--- b: {}",functionDto.getB());
        LOGGER.info("--- c: {}",functionDto.getC());


        if(functionDto.getA() == null || functionDto.getB() == null ){
            return new ResponseEntity<>(new InformAboutEquals(),HttpStatus.BAD_REQUEST);
        }else{
            service.CheckFunction(functionDto);
            return  new ResponseEntity<Object>(service.CheckFunction(functionDto), HttpStatus.OK);
        }
    }
    
    


}

/*
Na podstawie utworzone zadania proszę rozbudować aplikację. Zamień Get Na post - będziemy teraz wysyłać CIAŁO (nie parametry) ze zmiennymi a,b,c. Zaimplementuj też kontrolę czy a,b są null. Jeżeli ich nie ma wysyłaj odpowiedź serwera

{
  "alert":
    "podaj a",
    "podaj b"
}

Jeżeli nie są puste serwer ma zwracać wierzchołek funkcji (w) oraz p i q
 */
